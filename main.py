import os
import mel_parser
from analyzer import Analyzer


def main():
    prog = '''
        int g = 1 - 2 - 3;
        int t = 0;
        
        int[] kek = new int[]{ 1, 2, 3, 4, 5 };
        
        
        int[] what(int a, int b) 
        {
            int gh = 100 * a - b / 23;
            char e = 'a';
            int[] c = new int[]{5, 4, 6, 5, 1};
            return c;
        }
        
        
        int whatElse(int a, int b) 
        {
            char h = 10;
            int eb = 'b';
            int[] c = what(a, b);
            c[a + b] = 0;
            int e = c[3];
            while (true)
            {
                a = a + b;
                do
                {
                    b = a - b * b;
                }
                while (g + t > t)
            }
            return 10;
        }
    '''
    prog = mel_parser.parse(prog)
    a = Analyzer()
    the_prog, _ = a.analyze(prog)
    print(*the_prog.tree, sep=os.linesep)


if __name__ == "__main__":
    main()
